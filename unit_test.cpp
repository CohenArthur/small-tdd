#include <stdlib.h>
#include <err.h>

#include "c/tdd.h"
#include "cpp/tdd.hpp"

static void test_assert_pointer() {
	char *alert = "test_assert_pointer()";
	UnitTest test_pointer(alert);

	int *random_pointer = (int *) malloc(sizeof(int));
	if (!random_pointer)
		errx(1, "malloc failed");

	assert_valid_pointer(random_pointer, alert);
	assert_pointer_diff(random_pointer, NULL, alert);
	assert_pointer(random_pointer, random_pointer, alert);
	
	test_pointer.assert_valid_pointer(random_pointer);
	test_pointer.assert_pointer_diff(random_pointer, NULL);
	test_pointer.assert_pointer(random_pointer, random_pointer);
}

static void test_suite() {
	test_assert_pointer();
}

int main(void) {
	test_suite();	
}
