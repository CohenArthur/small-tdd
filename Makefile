ifdef DBG
CFLAGS=-D_DBG -g3
endif

CC=gcc
CXX=g++

CFLAGS+=-Wall -Wextra -pedantic

all: test_c test_cpp

test_c: c/tdd.c test.c
	$(CC) $(CFLAGS) -std=c11 -o test_c test.c c/tdd.c

test_cpp: cpp/tdd.cpp test.cpp
	$(CXX) $(CFLAGS) -o test_cpp test.cpp cpp/tdd.cpp

test: c/tdd.c cpp/tdd.cpp unit_test.cpp
	$(CPPC) -o unit_testing unit_test.cpp c/tdd.c cpp/tdd.cpp

clean:
	${RM} test_c test_cpp unit_testing *.o *.d
